from django.urls import path , include
from . import views
urlpatterns = [
    path('' , views.home_page),
    path('calculator/' , include('calc.urls')),
    path('book-info/<int:x>' , views.book_info)
]
