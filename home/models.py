from django.db import models


# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=255)
    price = models.FloatField()
    text  = models.TextField(max_length=5000 , blank=True , null=True , default='')
    count_pages = models.PositiveIntegerField()
    is_sale = models.BooleanField(default=False)
    percent_sale = models.FloatField()

    def __str__(self):
        return '{} {}$ {}'.format(self.title[:20] + '...'
             if len(self.title) > 20  else self.title,
            self.price ,
            self.is_sale)
