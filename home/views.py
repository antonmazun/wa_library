from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from .models import Book

# Create your views here.

def home_page(request):
    ctx  = {}
    # select * from home_book;

    all_books = Book.objects.all().order_by('-id')
    for book in all_books:
        print(book.title)
    ctx['books'] = all_books
    ctx['tab'] = 'home'
    return render(request,
                  'home/home_page.html',
                  ctx)

def book_info(request , x):
    book = Book.objects.get(id=x)
    return render(request , 'home/book.html', {
        'book': book
    })

