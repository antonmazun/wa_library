from django.urls import path , include
from . import views


urlpatterns = [
    path('' , views.example_home),
    path('<int:pk>' , views.update_obj),
    path('table/' , views.show_table),
    path('form-handler/' , views.form_handler),
    path('update_row/' , views.update_row),
    path('delete_row/<int:asd>' , views.delete_row)
]
