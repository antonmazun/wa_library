from django import forms
from .models import TestExample


class TestExampleForm(forms.ModelForm):
    class Meta:
        model = TestExample
        fields  = "__all__"
        widgets = {
            'char': forms.TextInput(attrs={'placeholder': 'Name'}),
            # 'int_f': forms.TextInput(attrs={'pattern': 'Name'}),
        }
        # exclude = ['char']