from .forms import SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, HttpResponseRedirect


def login_(request):
    ctx = {}
    if request.method == 'GET':
        return render(request, "auth_app/login_form.html", ctx)
    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('/')
        else:
            ctx['error'] = 'Неверный логин или пароль.'
            return render(request, "auth_app/login_form.html", ctx)


def logout_(request):
    logout(request)
    return redirect('/')


def register_(request):
    ctx = {}
    if request.method == 'GET':
        ctx['form'] = SignUpForm()
        return render(request, 'auth_app/register_form.html', ctx)
    elif request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('/')
        else:
            ctx['form'] = SignUpForm(request.POST)
            return render(request, 'auth_app/register_form.html', ctx)
