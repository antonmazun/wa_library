from django.urls import path, include
from . import views, auth_views

app_name = 'client'

urlpatterns = [
    path('', views.index),
    path('test-form/', views.test_form),
    path('cabinet/', views.cabinet, name='cabinet'),
    path('write_post/', views.write_post, name='write_post'),
    path('my_articles/', views.my_articles, name='my_articles'),
    path('articles/', views.articles, name='my_articles'),
    path('delete-book/<int:pk>', views.delete_book, name='my_articles'),
    path('change-book/<int:pk>', views.change_book, name='my_articles'),
    path('book-detail/<int:pk>', views.book_detail, name='my_articles'),
    path('set-to-wish-list/', views.set_to_session, name='set-to-wish-list'),
    path('wish-list/', views.wish_list, name='wish-list'),
    path('delete_from_sess/<int:pk>', views.delete_from_sess, name='delete_from_sess'),

]
auth_urls = [
    path('login/', auth_views.login_, name='login'),
    path('register/', auth_views.register_, name='register'),
    path('logout/', auth_views.logout_, name='logout'),
]

urlpatterns += auth_urls
