from django.shortcuts import render, redirect
from .forms import AuthorForm, ArticleForm, CommentForm
from .models import Author, Article
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


# Create your views here.


def index(request):
    return render(request, 'auth_app/main.html', {})


@login_required(login_url='/user/login')
def cabinet(request):
    ctx = {}
    ctx['cabinet_tab'] = 'main'
    if request.method == 'GET':
        try:
            author = Author.objects.get(user=request.user.id)
            ctx['form'] = AuthorForm(instance=author)
        except Exception as e:
            ctx['form'] = AuthorForm
    elif request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = Author.objects.get(user=request.user.id)
            author.bio = form.cleaned_data['bio']
            author.type_view = form.cleaned_data['type_view']
            author.pseudoname = form.cleaned_data['pseudoname']
            author.save()
            ctx['save'] = True
            ctx['form'] = AuthorForm(instance=author)
    return render(request, 'auth_app/cabinet.html', ctx)


def write_post(request):
    ctx = {}
    ctx['article_form'] = ArticleForm
    ctx['cabinet_tab'] = 'write_post'
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            author = Author.objects.get(user=request.user.id)
            form.save(author=author)
        else:
            ctx['article_form'] = form
    return render(request, 'auth_app/write_post.html', ctx)


def my_articles(request):
    ctx = {}
    ctx['cabinet_tab'] = 'my_articles'
    aut = Author.objects.get(user=request.user.id)
    ctx['all_articles'] = Article.objects.filter(author=aut)
    return render(request, 'auth_app/my_articles.html', ctx)


def delete_book(requst, pk):
    try:
        Article.objects.get(pk=pk).delete()
    except Exception as e:
        print(e)
    return redirect('/user/my_articles/')


def articles(request):



    return render(request, 'auth_app/articles.html', {
        'list': Article.objects.filter(status='publish')
    })


def set_to_session(request):
    saved_list = request.session.get('read_article', [])
    saved_list.append(int(request.GET.get('article_id')))
    request.session['read_article'] = saved_list
    return redirect('/user/articles/')


def wish_list(request):
    ctx = {}
    article_ids = request.session.get('read_article', [])
    print(article_ids)
    ctx['all_articles'] = [Article.objects.get(pk=_id) for _id in article_ids]
    return render(request, 'auth_app/wish_list.html', ctx)


def delete_from_sess(request, pk):
    article_ids = request.session.get('read_article', [])
    if pk:
        print(pk, '----------------')
        if pk in article_ids:
            article_ids.remove(pk)
            request.session['read_article'] = article_ids
    print()
    return redirect('/user/wish-list/')


def book_detail(request, pk):
    return render(request, 'auth_app/book.html', {
        'article': Article.objects.get(pk=pk)
    })


def change_book(request, pk):
    article = Article.objects.get(pk=pk)
    if request.method == 'GET':
        form = ArticleForm(instance=article)
        return render(request, 'auth_app/change_article.html', {
            'form': form
        })
    elif request.method == 'POST':
        form = ArticleForm(request.POST, instance=article)
        if form.is_valid():
            author = Author.objects.get(user=request.user.id)
            form.save(author=author)
        return redirect('/user/my_articles/')


from .forms import TestForm


def test_form(request):
    ctx = {}
    if request.method == 'GET':
        ctx['form'] = TestForm
        return render(request,
                      'auth_app/test_model_form.html',
                      ctx)

    elif request.method == 'POST':
        form = TestForm(request.POST)

        if form.is_valid():
            print(form.custom_method())
            form.save()
            print(form.cleaned_data)
            pass
        else:
            ctx['form'] = TestForm(request.POST)
        return render(request,
                      'auth_app/test_model_form.html',
                      ctx)
